package cprojet_tp2_Courrier;



import static org.junit.jupiter.api.Assertions.*;//pour pouvoir utiliser les assertions 

import java.util.stream.Stream;

//les annotations sont apparu dans junit5

import org.junit.Test;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.junit.runners.Parameterized;


public class testCourrier {
	
	//dans une classe de test on s'en fou que les attributs soit privé ou pas 
	//assertEquals(valeur attendu , valeur constatée) ;
	//"Failures" c'est quand on a une attente particulière non validée 
	//"Errors" c'est quand on a une execption qui n'était pas prevu y'a même pas d'assert violé juste une execption
	//non anticipé ne jamais remonté une erreur en dehors des méthode de teste on doit mettre un "assertThrows"
	
	//on déclare toujours à l'exterieur des méthodes 
   public Courrier courrier1;
   public Courrier courrier2;
   public Courrier courrier3;
   public Courrier courrier4;
   //on fait d'abord la methode d'initialisation d'instance
	@BeforeEach
	public  void initCourrier() {
		 Courrier courrier1=new Courrier("djouher2000m@gmail.com","question cour",
				"bonjour la question est sur la Pj ci-dessous","https://piecej.com");	
		courrier2=new Courrier("djouher2000m@gmail.com","",
				"bonjour la question est sur la Pj ci-dessous","https://piecej.com");
	     courrier3=new Courrier("djouhermoulahcene@gmail.fr","saly","pourquoi quoi pas de piece","https://piecej.com");
		
	}
   /*
	//teste la présence du titre dans le courrier 1 si ce qu'il y'a entre parenthèse est faux il retourne une exception
	@Test
	public void testPresenceTitre() {
	 assertTrue(courrier3.titrePresent());	
	}
	
	//si le teste marche ca veut dire on a bien ce qu'il y'a en param retourne faux comme l'indique le assert
	@Test
	public void testNonPresenceTitre() {
		assertFalse(courrier2.titrePresent());
	}
	
	@Test
	public void testadresseElecDfbf() {
		assertTrue(courrier3.adressevalide());
	}
	
	@Test
	public void testmessageAvecPj() {
		assertTrue(courrier1.messageAvecPj());
	}
	
	
   /* les tests paramétrés permet réutiliser des méthodes de testes avec une listes de mots pour 
    * l'objet sur lequel ce fait le teste en remplacant ce qui est passer en parametre dans l'attribut qu'on 
    * veux changer*/
	
   //on met ces jeux de mots dans une méthode annoter @parameters
   //cette méthode retourne une collection de tableaux contenant les données et éventuellement le résultat attendu 
    
   
   //@Test
	@ParameterizedTest
	@ValueSource(strings= {""})
	public void testPresenceTitreP(String titre) {
		courrier4=new Courrier("djouher2000m@gmail.com",titre,"bonjour la question est sur la Pj ci-dessous","https://piecej.com");
    	
    	//assertTrue(courrier1.titrePresent());
		assertTrue(courrier4.titrePresent()) ;
		}
    
    //tel que pour chaque valeur de "strings" il fait le teste 
    //là il remplace chaque "adresse" par les valeurs de strings 
   
   @ParameterizedTest
    @ValueSource(strings = {"mdjfhhg","########","coucou","@@@@@@"})
    public void testadresseElecDfbfP(String adresse) {
    	courrier4=new Courrier(adresse,"question cour","bonjour la question est sur la Pj ci-dessous","https://piecej.com");
    	//assertTrue(courrier1.adressevalide());
    	assertFalse(courrier4.adressevalide());
    }
   
    //@Test
   @ParameterizedTest
   @ValueSource(strings = {"sjknnff","bonjour t'es pjnon","biensur c'est un faux pieces"})
    public void testmessageAvecPjP(String message) {
	   courrier4=new Courrier("djouher@gmail.com","question cour",message,"https://piecej.com");
    	//assertTrue(courrier1.messageAvecPj());
	   assertFalse(courrier4.messageAvecPj());
    }
   
   //on teste pour chaque valeur de la liste 
    // {0} - la première valeur de paramètre de cette invocation du test , c'est pour prendre le premier argument de
   //Arguments du fournisseurs de données 
   //{1} - la deuxième valeur du paramètre
   
   @ParameterizedTest(name="{index} : adresse={0} reason={1}")
   @MethodSource("stringAvecRaison") //là on lie la methode de test paramétré avec le fournisseurs de données "stringAvecRaison"
   public void testParamAvecRaison(String Adresse,String raison) {
	    courrier4=new Courrier(Adresse,"question cour","bonjour la question est sur la Pj ci-dessous","https://piecej.com");
	   assertFalse(courrier4.adressevalide(),raison);
   }

   
   static Stream<Arguments> stringAvecRaison() {
	    return Stream.of(
	    		Arguments.of("@@@@@@","pas une adresse"),
	    		Arguments.of("#####","adresse mal ecrite"),
	    		Arguments.of("djouher@","adresse incomplète"));
	}
	
}
