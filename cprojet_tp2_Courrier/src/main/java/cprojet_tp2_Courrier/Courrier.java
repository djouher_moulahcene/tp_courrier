package cprojet_tp2_Courrier;

import java.io.File;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.regex.*; //c'est pour pouvoir utiliser les expressions régulières

import com.fasterxml.jackson.databind.ObjectMapper;


public class Courrier {
 private String adresseElecD;
 private String titre;
 private String corpsMessage;
 private String pieceJ;
 
 
 //pattern cette classe a pour objet de compiler l'expression réguliere fournie .
 /*matcher cette classe permet de comparer une exp regulière à un texte et de faire 
 différentes opérations dessus avec la methode static " matches " */ 
 
 //constructeur 
 public Courrier(String adresseElecD,String titre,String corpsMessage,String pieceJ) {
	 this.adresseElecD=adresseElecD;
	 this.titre=titre;
	 this.corpsMessage=corpsMessage;
	 this.pieceJ=pieceJ;
 }
 
 
 public String getAdresseElecD() {
	return adresseElecD;
}


public void setAdresseElecD(String adresseElecD) {
	this.adresseElecD = adresseElecD;
}


public String getTitre() {
	return titre;
}


public void setTitre(String titre) {
	this.titre = titre;
}


public String getCorpsMessage() {
	return corpsMessage;
}


public void setCorpsMessage(String corpsMessage) {
	this.corpsMessage = corpsMessage;
}


public String getPieceJ() {
	return pieceJ;
}


public void setPieceJ(String pieceJ) {
	this.pieceJ = pieceJ;
}


//"indexOf" renvoie le nombre de fois qu'apparait ce qui est en param 
 /* \W signifie non alpahnumérique */
 public boolean adressevalide() {
	 
	 if (adresseElecD.matches("^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$")) {
		 return true;
	 }
	 else return false ;
 }
 public boolean titrePresent() {
	 return (titre != "");
 }
	 
public boolean messageAvecPj() { 
	 if ((corpsMessage.indexOf("Pj")> 0) || (corpsMessage.indexOf("joint")>0) || (corpsMessage.indexOf("jointe")>0)) {
		 return true;
	 }
	 else return false ;
}
	 

 

public static void main(String[] args) {
	//d'un objet java vers json
	ObjectMapper objectMapper = new ObjectMapper();
	ArrayList<Courrier> lis=new ArrayList<Courrier >();
	Courrier courrier1=new Courrier("djouher2000m@gmail.com","question cour",
			"bonjour la question est sur la Pj ci-dessous","https://piecej.com");	
	Courrier courrier2=new Courrier("djouher2000m@gmail.com","",
			"bonjour la question est sur la Pj ci-dessous","https://piecej.com");
     Courrier courrier3=new Courrier("djouhermoulahcene@gmail.fr","saly","pourquoi quoi pas de piece","https://piecej.com");
	
	lis.add(courrier1);
	lis.add(courrier2);
	lis.add(courrier3);
	try {
		
		objectMapper.writeValue(new File("target/courrier.json"),lis);
		
} catch (Exception e) {
	e.printStackTrace();
}
	
//déserialiser de json vers objet 
	try {
	
		Courrier [] cour=objectMapper.readValue(Paths.get("target/courrier.json").toFile(), 
				Courrier[].class);
		for(int i=0; i<cour.length; i++) {
		System.out.println(cour[i]); }//ca appel implicitement la méthode toString 
	} catch (Exception e) {
		e.printStackTrace();
	}

}
 
}
